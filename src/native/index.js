const native = require("bindings")("nativelib");
const fs = require("fs");
native.add_seach_path("/usr/share/pixmaps/");
native.add_seach_path("/usr/share/icons/hicolor/48x48/apps/");
native.add_seach_path("/usr/share/icons/ukui/48x48/apps/");

exports.get_icon = function getIcon(filename, size, unkownicon) {
  if (filename.endsWith(".desktop")) {
    return native.get_desktop_icon(filename, unkownicon);
  } else {
    let iconpath = native.get_ndesktop_icon(filename, size);
    if (iconpath == "unknown") return unkownicon;
    if (!iconpath.includes("gnome")) return iconpath;
    let ukuipath = iconpath.replace("/gnome/", "/ukui/");
    return fs.existsSync(ukuipath) ? ukuipath : iconpath;
  }
};
exports.get_desktop_name = function(filename, unkown) {
  return native.get_desktop_name(filename, unkown);
};
exports.get_desktop_exec = function(filename) {
  return native.get_desktop_exec(filename);
};
exports.get_type = function(filename) {
  return native.get_type(filename);
};
exports.get_recommended_open_way = function(path) {
  return native.get_recommended_open_way(path);
};
