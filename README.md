# 介绍
本项目为2021麒麟杯参赛作品，开发以及测试时的系统环境为ukui 3.0, 遵循GPL 3.0开源协议。开发的初衷是满足日常生活中个人用户比较关注的几个关键性功能，例如待办事项、定时提醒、备忘录、壁纸库、桌面收纳功整理、音乐播放、天气等功能。
# 使用
## 一.安装相关依赖
### 1.首先克隆整个项目，您可以选择使用以下命令，也可以直接通过网页版直接下载zip包
```
git clone https://gitee.com/yeyeyeping/desktop-tool.git
```
### 2.本项目获取桌面图标等部分借助c++使用gtk库实现，并且打包成了npm module,因此运行需要先将native部分编译，并安装到项目中
### 2.1.安装gtk
```
sudo apt-get install gnome-devel gnome-devel-docs
```
### 2.2.进入项目目录，执行下面命令安装本地native模块
```
npm install --save ./src/native 
```
### 3.安装其他依赖
执行这一步的时候推荐先设置淘宝源，以防electron下载失败
```
npm config set registry https://registry.npm.taobao.org/
npm config set ELETRON_MIRROR https://npm.taobao.org/mirrors/electron/
```
```
npm install
```

## 二.启动
```
npm start
```